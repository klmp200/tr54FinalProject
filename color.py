from pybricks import ev3brick as brick

green_h, green_s, green_v = 0, 0, 0
orange_h, orange_s, orange_v = 0, 0, 0

# setup calibration's values
def calibrate(robot):
    global green_h, green_s, green_v
    global orange_h, orange_s, orange_v
    brick.display.clear()
    brick.display.text('setup green color')
    while not robot.pressed():
        brick.display.text('setup green color')
    while robot.pressed():
        pass
    r, b, g = robot.color()
    green_h, green_s, green_v = rgb2hsv(r, g, b)
    brick.display.clear()
    brick.display.text('setup orange color')
    while not robot.pressed():
        brick.display.text('setup orange color')
    while robot.pressed():
        pass
    r, b, g = robot.color()
    orange_h, orange_s, orange_v = rgb2hsv(r, g, b)
    brick.display.clear()
    brick.display.text('wait for start')
    while not robot.pressed():
        brick.display.text('wait for start')
    while robot.pressed():
        pass

# convert rgb to hsv
def rgb2hsv(r, g, b):
    r, g, b = r/100.0, g/100.0, b/100.0
    mx = max(r, g, b)
    mn = min(r, g, b)
    df = mx-mn
    if mx == mn:
        h = 0
    elif mx == r:
        h = (60 * ((g-b)/df) + 360) % 360
    elif mx == g:
        h = (60 * ((b-r)/df) + 120) % 360
    elif mx == b:
        h = (60 * ((r-g)/df) + 240) % 360
    if mx == 0:
        s = 0
    else:
        s = df/mx
    v = mx
    return h, s, v

# check if color (h,v,s) is similar to the color (t_h,t_s,t_v)
def check_color(h, s, v, t_h, t_s, t_v):
    clearance_h, clearance_s, clearance_v = 30, 0.15, 0.15

    cond_h, cond_s, cond_v = False, False, False
    if (t_h-clearance_h)>=0 and (t_h+clearance_h)<=360:
        if (t_h-clearance_h)<=h<=(t_h+clearance_h):
            cond_h = True
    elif (t_h-clearance_h)>=0:
        if (t_h-clearance_h)<=h or h<=(t_h+clearance_h-360):
            cond_h = True
    else:
        if (t_h-clearance_h+360)<=h or h<=(t_h+clearance_h):
            cond_h = True
    if (t_s-clearance_s)<=s<=(t_s+clearance_s):
        cond_s = True
    if (t_v-clearance_v)<=v<=(t_v+clearance_v):
        cond_v = True

    return cond_h and cond_s and cond_v

# detecte green and orange
def hsv2color(h,s,v):
    if check_color(h, s, v, green_h, green_s, green_v):
        return 'green'
    elif check_color(h, s, v, orange_h, orange_s, orange_v):
        return 'orange'
    else:
        return 'unkown'

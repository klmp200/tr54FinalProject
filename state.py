from color import rgb2hsv, hsv2color
from pybricks.tools import print, wait, StopWatch
from pybricks import ev3brick as brick
from pybricks.parameters import Color

class State:

    def __init__(self, danger_begin = 30, danger_end = 80, green_length = 165.32, orange_length = 174.08):
        self.previous_color = 'unkown'
        self.last_level_change = 0
        self.same_color_counter = 0

        self.danger_begin = danger_begin
        self.danger_end = danger_end
        self.green_length = green_length
        self.orange_length = orange_length

        self.color = 'unkown'
        self.state = -1
        self.state_changed = False

        self.clock = StopWatch()
        self.clock.reset()
    
    # update robot's state with data from the color sensor
    def update(self, r, g, b, distance):
        time = self.clock.time()
    
        h, s, v = rgb2hsv(r, g, b)
        color = hsv2color(h, s, v)
        
        if self.previous_color == color:
            self.same_color_counter += 1
        else:
            self.same_color_counter = 0
        
        #update last color
        if color == 'orange' and (time-self.last_level_change) > 500 and self.same_color_counter >= 3:
            self.last_level_change = time
            brick.sound.beep()
        if color == 'green' and (time-self.last_level_change) > 500 and self.same_color_counter >= 3:
            self.last_level_change = time
            brick.sound.beep()
        
        # update color
        if color != self.previous_color:
            if color == 'green' or color == 'orange':
                self.color = color
            if color == 'green':
                brick.light(Color.GREEN)
            elif color == 'orange':
                brick.light(Color.ORANGE)
            else:
                brick.light(Color.BLACK)
            
            if color == 'green' or color == 'orange':
                # update path length
                if self.color == 'green':
                    length = self.green_length
                elif self.color == 'orange':
                    length = self.orange_length
                # reset state
                self.state = 0
                self.state_changed = True
            self.previous_color = color
        
        # update state
        if not self.state_changed:
            if self.state == 0 and distance > self.danger_begin:
                self.state = 1
                self.state_changed = True
            elif self.state == 1 and distance > self.danger_end:
                self.state = 2
                self.state_changed = True

    def get_state(self):
        return self.color, self.state
    
    def has_change(self):
        changed = self.state_changed
        self.state_changed = False
        return changed
    
    # calculate distance until the next intersection
    def get_danger_distance(self, distance):
        if self.state == 0:
            return self.danger_begin - distance
        elif self.state == 1 or self.state == 2:
            length = 0
            if self.color == 'green':
                length = self.green_length
            elif self.color == 'orange':
                length = self.orange_length
            return length - distance + self.danger_begin
        else:
            return -1


import time

t0 = 0

class SyncTime:
    def __init__(self):
        pass

    def time(self):
        return time.time() - t0

    @staticmethod
    def update(real_time):
        t0 = time.time() - real_time
        
import json
import time
import random
import socket

# get ip addresse
def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(("10.255.255.255", 1))
        IP = s.getsockname()[0]
    except:
        IP = "127.0.0.1"
    finally:
        s.close()
    return IP

# calculate a checksum of a byte array
def checksum(data):
    assert type(data) == bytes
    return (~sum([0xFF] + list(data)) + 1) & 0xFF

# generate a random id (size n)
def random_id(n=17):
    random.seed(int(time.time() * 1000000))
    base64 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+/"
    rand_id = [base64[random.randint(0, 63)] for _ in range(n)]
    return "".join(rand_id)

# create a packet
def serialize(_id, cmd, _time=None, data={}):
    assert type(_id) == str and len(_id) == 17
    assert type(cmd) == str
    assert _time is None or type(_time) == float
    assert type(data) == dict

    if _time is None:
        _time = time.time()

    msg = {"id": _id, "time": _time, "cmd": cmd, "data": data}
    enc = bytes(json.dumps(msg), "utf-8")

    return bytes(list(enc) + [checksum(enc)])

# decode a packet
def unserialize(data):
    assert type(data) == bytes

    if checksum(data[:-1]) != data[-1]:
        raise Exception("Checksum error")

    result = json.loads(data[:-1].decode("utf-8"))

    _id = result["id"]
    if type(_id) != str and len(_id) != 17:
        raise ValueError("invalid id {}".format(_id))

    _time = result["time"]
    if type(_time) != float:
        raise ValueError("invalid time {}".format(_time))

    cmd = result["cmd"]
    if type(cmd) != str:
        raise ValueError("invalid cmd {}".format(cmd))

    data = result["data"]
    if type(data) != dict:
        raise ValueError("invalid data {}".format(data))

    return _id, _time, cmd, data

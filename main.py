#!/usr/bin/env pybricks-micropython
from pybricks import ev3brick as brick
from pybricks.ev3devices import (
    Motor,
    TouchSensor,
    ColorSensor,
    InfraredSensor,
    UltrasonicSensor,
    GyroSensor,
)
from pybricks.parameters import (
    Port,
    Stop,
    Direction,
    Button,
    Color,
    SoundFile,
    ImageFile,
    Align,
)
from pybricks.tools import print, wait, StopWatch
from pybricks.robotics import DriveBase

import sys
import random
import os
import math
import gc

import time

from robot import Robot
from log import Log
from color import rgb2hsv, hsv2color, calibrate
from guidance import Guidance, PidLine, PidSpeed, PidDistance
from state import State
from drive import Drive, ConstantSpeed, SmoothStop
from client import Client
from sync_time import SyncTime
import settings
from net_common import random_id, serialize, unserialize, get_ip

settings.load()


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError:
        pass


mkdir_p("data")

file_name = "data/log.csv"

sync_time = SyncTime()
client = Client()
robot = Robot()
guidance = Guidance()
pid_line = PidLine(
    target=settings.settings["pid_line"]["target"],
    p=settings.settings["pid_line"]["p"],
    i=settings.settings["pid_line"]["i"],
    d=settings.settings["pid_line"]["d"],
)
pid_speed = PidSpeed(
    p=settings.settings["pid_speed"]["p"],
    i=settings.settings["pid_speed"]["i"],
    d=settings.settings["pid_speed"]["d"],
)
state = State(
    danger_begin=settings.settings["road"]["danger_begin"],
    danger_end=settings.settings["road"]["danger_end"],
    green_length=settings.settings["road"]["green_length"],
    orange_length=settings.settings["road"]["orange_length"],
)
drive = Drive(
    state=state,
    default_speed=settings.settings["driving"]["default_speed"],
    safety_stop_distance=settings.settings["driving"]["safety_stop_distance"],
    safety_clearance_distance=settings.settings["driving"]["safety_clearance_distance"],
)
log = Log(file_name)
log.clear()

# color calibration
calibrate(robot)

clock = StopWatch()
clock.reset()

brick.light(Color.BLACK)

pid_line.reset_clock()
pid_speed.reset_clock()

prev_color = None
distance_green = 0
distance_orange = 0

while not robot.pressed():
    # update guidance algorithme
    t0 = time.time()
    guidance.update(robot.motor_right.angle(), robot.motor_left.angle())
    distance = guidance.get_distance()

    # update server's info
    if client.targeted_time is not None:
        drive.set_targeted_time(client.targeted_time)
    drive.set_right_of_way(client.right_of_way)

    # distance sensor
    distance_sensor = robot.distance()
    # light sensor
    r, b, g = robot.color()
    light = (r + g + b) / 300.0

    # update drive
    target = drive.update(
        r, g, b, distance_sensor, distance, sync_time.time(), guidance.get_speed()
    )

    # update speed pid
    current_speed = (
        (5.5 * math.pi) * (robot.motor_left.speed() + robot.motor_right.speed()) / 360.0
    )
    pid_speed.set_target(target)
    speed = pid_speed.update(current_speed)

    # update line pid
    pid_line.set_speed(speed)
    speed_left, speed_right = pid_line.update(light)
    robot.motor_left.dc(speed_left)
    robot.motor_right.dc(speed_right)

    # communication tasks
    color, state = drive.state.get_state()
    if color != prev_color:
        if color == "green":
            distance_green = distance
        elif color == "orange":
            distance_orange = distance
        prev_color = color
    if clock.time() > settings.settings["logs"]["interval"] and client.message is None:
        if state != -1:
            client.send_position(
                drive.state.get_danger_distance(drive.distance), color, state
            )
        clock.reset()
        t1 = time.time()

    # save logs
    gc.collect()

import math
from pybricks.tools import print, wait, StopWatch

class Guidance:
    #wheel diameter in cm
    def __init__(self, wheel_size = 5.5):
        self.distance = 0
        self.prev_right = 0
        self.prev_left = 0
        self.speed = 0
        self.wheel_size = wheel_size

        self.clock = StopWatch()
        self.clock.reset()
        self.last_time = self.clock.time()
    
    def set_angles(self, right, left):
        self.prev_right = right
        self.prev_left = left

    # update distance estimator from right and left wheels position
    def update(self, right, left):
        #delta angle in rad
        d_angle_right = (right-self.prev_right)*math.pi/180.
        d_angle_left = (left-self.prev_left)*math.pi/180.

        #dt mesure
        time = self.clock.time()
        dt = (time - self.last_time) / 1000.0
        self.last_time = time

        #delta distance in cm
        d_dist_right = d_angle_right*self.wheel_size
        d_dist_left = d_angle_left*self.wheel_size

        #update speed
        self.speed = 0.5*(d_dist_right+d_dist_left)/dt

        #update distance
        self.distance += 0.5*(d_dist_right+d_dist_left)

        #update previous values
        self.prev_right = right
        self.prev_left = left

    def get_distance(self):
        return self.distance
    
    def set_distance(self,dist):
        self.distance = dist

    def get_speed(self):
        return self.speed

def in_range(x,_min,_max):
    return max(min(x,_max),_min)

class PidLine:
    def __init__(self,speed = 75, target = 0.4, p = 200, i = 0, d = 1):
        self.speed = speed
        self.target = target
        self.p = p
        self.i = i
        self.d = d

        self.clock = StopWatch()
        self.clock.reset()

        self.last_error = 0
        self.integ_error = 0
        self.last_time = self.clock.time()
    
    def set_target(self, target):
        self.target = target

    def reset_time(self):
        self.last_time = self.clock.time()
    
    def set_speed(self, speed):
        self.speed = speed

    def reset_clock(self):
        self.last_time = self.clock.time()

    # get pid responce from sensor value
    def update(self, value):
        #dt mesure
        time = self.clock.time()
        dt = (time - self.last_time) / 1000.0
        self.last_time = time

        #error calculation
        error = value - self.target
        delta_error = (error - self.last_error) / dt
        self.integ_error += error * dt
        self.last_error = error

        #pid calculation
        result = self.p*error + self.i*self.integ_error + self.d*delta_error

        #mix motor speed
        speed_left = self.speed + result
        speed_right = self.speed - result

        #keep in range 0-100
        speed_left = in_range(speed_left, 0, self.speed)
        speed_right = in_range(speed_right, 0, self.speed)

        return speed_left, speed_right

class PidSpeed:
    def __init__(self,target = 0, p = 7, i = 40, d = 0):
        self.target = target
        self.p = p
        self.i = i
        self.d = d

        self.clock = StopWatch()
        self.clock.reset()

        self.last_distance = 0

        self.last_error = 0
        self.integ_error = 0
        self.last_time = self.clock.time()
    
    def set_target(self, target):
        if target == 0:
            self.integ_error = 0
        self.target = target

    def reset_clock(self):
        self.last_time = self.clock.time()

    # get pid response from current speed
    def update(self, speed):
        #dt mesure
        time = self.clock.time()
        dt = (time - self.last_time) / 1000.0
        self.last_time = time

        #error calculation
        error = self.target - speed
        delta_error = (error - self.last_error) / dt
        self.integ_error += error * dt
        self.last_error = error

        #pid calculation
        result = self.p*error + self.i*self.integ_error + self.d*delta_error

        #keep in range 0-100
        cmd = in_range(result, 0, 100)

        return cmd

class PidDistance:
    def __init__(self,target = 0, p = 50, i = 0, d = 3):
        self.target = target
        self.p = p
        self.i = i
        self.d = d

        self.clock = StopWatch()
        self.clock.reset()

        self.last_distance = 0

        self.last_error = 0
        self.integ_error = 0
        self.last_time = self.clock.time()
    
    def set_target(self, target):
        self.target = target

    # get pid response from current distance
    def update(self, distance):
        #dt mesure
        time = self.clock.time()
        dt = (time - self.last_time) / 1000.0
        self.last_time = time

        #error calculation
        error = self.target - distance
        delta_error = (error - self.last_error) / dt
        self.integ_error += error * dt
        self.last_error = error

        #pid calculation
        result = self.p*error + self.i*self.integ_error + self.d*delta_error

        #keep in range 0-100
        cmd = in_range(result, 0, 100)

        return cmd

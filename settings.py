from pybricks.tools import print
import json

settings = {}

def load(file_name = 'settings.json'):
    global settings
    with open(file_name, 'r') as f:
        text = f.read()
        settings = json.loads(text)
    
import _thread
import time
from pybricks.tools import print, wait
import socket

from sync_time import SyncTime
from net_common import random_id, serialize, unserialize, get_ip

# reception deamon
def reception(client):
    while True:
        try:
            data, addr = client.sock.recvfrom(256)
        except OSError:
            continue
        addr = ".".join([str(int(i)) for i in addr[4:8]])
        if addr == client.ip:
            continue
        _id, _time, cmd, data = unserialize(data)
        if _id == '00000000000000000':
            if cmd == 'time':
                SyncTime.update(_time)
        elif _id == client.id:
            if cmd == 'target':
                client.targeted_time = data['target']
                client.right_of_way = data['right_of_way']

# emission deamon
def emission(client):
    while True:
        if client.message is not None:
            try:
                message = serialize(client.id, 'pos', client.time.time(), data = client.message)
                client.sock.sendto(message, ('255.255.255.255', client.port))
                client.message = None
            except OSError:
                continue
        time.sleep(0.1)

class Client:
    def __init__(self, port = 37020):
        self.id = random_id()

        self.port = port
        self.ip = get_ip()

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.sock.bind(('', self.port))
    
        _thread.start_new_thread(reception, (self,))
        _thread.start_new_thread(emission, (self,))

        self.targeted_time = None
        self.right_of_way = False

        self.time = SyncTime()

        self.message = None
    
    def send_position(self, distance, color, state):
        self.message = {'position':distance, 'color':color, 'state':state}
    
    def close(self):
        self.sock.close()

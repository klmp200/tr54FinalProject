
from pybricks import ev3brick as brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import (Port, Stop, Direction, Button, Color,
                                 SoundFile, ImageFile, Align)
from pybricks.tools import print, wait, StopWatch
from pybricks.robotics import DriveBase

brick.display.image('unitec.png')

class Robot:
    def __init__(self):
        # init motor
        self.motor_left = Motor(Port.B, Direction.CLOCKWISE, [12,36])
        self.motor_right = Motor(Port.C, Direction.CLOCKWISE, [12,36])

        # init distance sensor
        try:
            self.distance_sensor = InfraredSensor(Port.S2)
        except OSError:
            try:
                self.distance_sensor = UltrasonicSensor(Port.S2)
            except OSError:
                raise Exception('No distance sensor found!')

        #init color sensor
        self.color_sensor = ColorSensor(Port.S3)

        #init touch sensor
        self.touch_sensor = TouchSensor(Port.S1)

        #init sensors values
        self.distance_value = self.distance_sensor.distance()
        self.color_value = self.color_sensor.rgb()
    
    def forward(self,speed):
        self.motor_left.run(speed)
        self.motor_right.run(speed)

    def rotate(self, angle, aSpeed):
        k = 0.65
        self.motor_left.run_angle(aSpeed,angle*k, Stop.COAST, False)
        self.motor_right.run_angle(aSpeed,-angle*k, Stop.COAST, False)
    
    def stop(self):
        self.motor_left.run(0)
        self.motor_right.run(0)
    
    def take_value(self):
        self.color_value = self.color_sensor.rgb()
        self.distance_value = self.distance_sensor.distance()

    def distance(self):
        self.distance_value = self.distance_sensor.distance()
        return self.distance_value

    def color(self):
        self.color_value = self.color_sensor.rgb()
        return self.color_value

    def pressed(self):
        return self.touch_sensor.pressed()
    
    def COME_ON(self):
        brick.sound.file('come_on.wav',vol)
    
    def status(self):
        brick.display.image('unitec.png')


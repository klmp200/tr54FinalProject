from state import State
from pybricks import ev3brick as brick
from pybricks.parameters import Color
from pybricks.tools import print


class ConstantSpeed:
    def __init__(self, speed):
        self.speed = speed

    def get_speed(self, distance):
        return self.speed


class SmoothStop:
    def __init__(self, max_speed, current_distance, stop_distance):
        self.max_speed = max_speed
        self.current_distance = current_distance
        self.stop_distance = stop_distance

    # calculate speed for a given distance
    def get_speed(self, distance):
        if self.stop_distance <= 0:
            return 0
        coef = self.max_speed / self.stop_distance
        target = self.current_distance + self.stop_distance
        res = coef * (target - distance)
        if res < 0:
            res = 0
        return res


class Drive:
    def __init__(
        self,
        state,
        default_speed=15.0,
        safety_stop_distance=20.0,
        safety_clearance_distance=10.0,
    ):
        # robot state
        self.state = state

        self.safety_stop_distance = safety_stop_distance  # cm
        self.safety_clearance_distance = safety_clearance_distance  # cm
        self.default_speed = default_speed  # cm/s
        self.prev_distance = 0.0  # cm
        self.distance = 0.0  # cm
        self.targeted_time = -1.0  # s

        self.right_of_way = False  #
        self.current_speed = None

    def set_targeted_time(self, targeted_time):
        self.targeted_time = targeted_time

    def set_right_of_way(self, right_of_way):
        self.right_of_way = right_of_way

    # update pid target
    # take sensors information
    # memorize robot's state informations
    def update(
        self,
        r,
        g,
        b,
        distance_sensor,
        total_distance,
        time,
        current_speed,
        force_update=False,
    ):

        # force reset
        if force_update:
            self.current_speed = None

        # distance since last mark
        self.distance = total_distance - self.prev_distance
        # distance until
        danger_distance = self.state.get_danger_distance(self.distance)

        # update state
        _, prev_area = self.state.get_state()
        self.state.update(r, g, b, self.distance)

        # update last distance
        color, area = self.state.get_state()

        if self.state.has_change():
            if prev_area == -1 and area != -1:
                self.current_speed = None
                
            if prev_area == 1 and area == 2:
                self.right_of_way = False

            if area == 0:
                self.prev_distance = total_distance
                # update distance
                # distance since last mark
                self.distance = total_distance - self.prev_distance
                # distance until
                danger_distance = self.state.get_danger_distance(self.distance)

        if area != -1:
            if (
                danger_distance < self.safety_stop_distance
                and (not self.right_of_way)
                and type(self.current_speed) != SmoothStop
            ):
                self.current_speed = None

        if self.right_of_way:
            if (
                type(self.current_speed) != ConstantSpeed
                or self.current_speed != self.default_speed
            ):
                self.current_speed = ConstantSpeed(self.default_speed)

        if self.current_speed is None:
            if area == -1:  # don't know where you are? run at default speed
                self.current_speed = ConstantSpeed(self.default_speed)
            elif danger_distance < self.safety_stop_distance and (
                not self.right_of_way
            ):  # must stop until
                if danger_distance < 0:
                    danger_distance = 0
                self.current_speed = SmoothStop(
                    current_speed, total_distance, danger_distance
                )
            elif self.targeted_time == -1.0:
                self.current_speed = ConstantSpeed(self.default_speed)
            elif self.targeted_time != -1:  # arrive on time
                speed = danger_distance / self.targeted_time
                if speed > self.default_speed:
                    speed = self.default_speed
                self.current_speed = ConstantSpeed(speed)
            else:  # kown location but not targeted time
                self.current_speed = ConstantSpeed(self.default_speed)
            
        speed = self.current_speed.get_speed(total_distance)

        # safty stop
        if distance_sensor < self.safety_clearance_distance * 20:
            error = 0.1 * distance_sensor - self.safety_clearance_distance
            coef = self.default_speed / self.safety_clearance_distance
            safe_speed = coef * error
            if speed > safe_speed:
                speed = safe_speed
            if speed < 0:
                speed = 0

        return speed

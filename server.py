#!/usr/bin/env python3
# -*- coding:utf-8 -*
import socket
import time
import sys

from termcolor import colored, cprint
from collections import OrderedDict

from net_common import *
import threading
import queue


class Server:
    """
        Handle communications with all robots
    """

    BROADCAST_ADDR = "255.255.255.255"
    MESSAGE_SIZE = 1024

    # Constants to enforce to robots
    SAFETY_MARGIN = 70
    WANTED_SPEED = 6

    def __init__(self, port=37020):
        # Define characteristics of the connection
        self.id = "00000000000000000"
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.sock.bind(("", port))
        self.ip = socket.gethostbyname(socket.gethostname())
        self.port = port

        # Queue storing messages waiting for the sending thread
        self.send_queue = queue.SimpleQueue()

        # Data structure storing informations about each robot
        self.positions_lock = threading.Lock()
        self.positions = (
            OrderedDict()
        )  # Contains {"last_update": time, "position": position} with _id as key

        # All thread initialization
        self.time_sync_service_thread = threading.Thread(target=self._time_sync_service)
        self.target_service_thread = threading.Thread(target=self._target_service)
        self.message_reciever_service_thread = threading.Thread(
            target=self._message_reciever_service
        )
        self.message_sender_service_thread = threading.Thread(
            target=self._message_sender_service
        )

    def display_packet(self, _id, _time, cmd, data):
        """
        Pretty print for packets
        """
        packet = "{:^20}{:^20}{:^20}{:^60}".format(
            str(_id), str(_time), str(cmd), str(data)
        )
        if cmd == "error":
            cprint(packet, "white", "on_red")
        elif _id == self.id:
            cprint(packet, "white")
        else:
            cprint(packet, "white", "on_green")

    def send_message(self, _id, cmd, data):
        """
        Add a message in the sending queue.
        """
        self.send_queue.put(
            {"_time": time.time(), "_id": _id, "cmd": cmd, "data": data}
        )

    def save_position(self, _id, _time, data):
        """
        Save and store the position of a robot
        """
        if "position" not in data or "state" not in data:
            return
        position = data["position"]
        if data["state"] == 1:
            position = 0
        self.positions_lock.acquire()
        self.positions[_id] = {"last_update": _time, "position": position}
        self.positions_lock.release()

    def __wait_message(self):
        """
        Blocking method. Used to wait and get a message
        Filter out all messages sent by the server itself
        """
        ip = self.ip
        while ip == self.ip:
            data, addr = self.sock.recvfrom(self.MESSAGE_SIZE)
            ip = addr[0]
        return data

    # Service definition
    def _time_sync_service(self):
        """
        Send the current time to all clients for time synchronization purpose
        """
        while True:
            self.send_message(self.id, "time", {})
            time.sleep(2)

    def _target_service(self):
        """
        Compute and send periodically time constraints for each robot
        """
        while True:
            time.sleep(0.5)
            messages = []
            expired = []

            current_time = time.time()
            last_position = None
            right_of_way = True

            self.positions_lock.acquire()
            # Reorder dict
            self.positions = OrderedDict(
                sorted(self.positions.items(), key=lambda item: item[1]["position"])
            )
            # Compute and send to each robot the time constraint and if they are allowed to pass
            for robot_id in self.positions:
                # Mark robots not connected since at least 10 seconds
                if current_time > self.positions[robot_id]["last_update"] + 10:
                    print("%s expired" % robot_id)
                    expired.append(robot_id)
                    continue

                if last_position:
                    last_position = last_position + self.SAFETY_MARGIN
                else:
                    last_position = self.positions[robot_id]["position"]

                messages.append(
                    {
                        "_id": robot_id,
                        "cmd": "target",
                        "data": {
                            "target": round(last_position / self.WANTED_SPEED, 3),
                            "right_of_way": right_of_way,
                        },
                    }
                )
                right_of_way = False
            # Remove infos about robots marked for removal
            for _id in expired:
                del self.positions[_id]
            self.positions_lock.release()
            for message in messages:
                self.send_message(**message)

    def _message_sender_service(self):
        """
        Thread in charge of sending messages inside the send queue
        """
        while True:
            if self.send_queue.empty():
                continue
            message = self.send_queue.get()
            serialized_message = serialize(**message)
            try:
                self.sock.sendto(serialized_message, (self.BROADCAST_ADDR, self.port))
                self.display_packet(**message)
            except Exception as e:
                print("Error sending message")
                print(e)

    def _message_reciever_service(self):
        """
        Thread in charge of processing incoming messages
        """
        while True:
            message = self.__wait_message()

            try:
                _id, _time, cmd, data = unserialize(message)
                self.display_packet(_id, _time, cmd, data)
            except Exception as e:
                print("Error unserializing data")
                print(e)
                continue
            if cmd == "pos":
                # Store position of robots
                self.save_position(_id, _time, data)

    def start(self):
        """
        Start the server
        """
        self.message_sender_service_thread.start()
        self.time_sync_service_thread.start()
        self.message_reciever_service_thread.start()
        self.target_service_thread.start()

        self.message_sender_service_thread.join()
        self.time_sync_service_thread.join()
        self.message_reciever_service_thread.join()
        self.target_service_thread.join()


Server().start()

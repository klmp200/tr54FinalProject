from pybricks import ev3brick as brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import (Port, Stop, Direction, Button, Color,
                                 SoundFile, ImageFile, Align)
from pybricks.tools import print, wait, StopWatch
from pybricks.robotics import DriveBase

class Log:
    def __init__(self, file_name = 'log.csv'):
        self.clock = StopWatch()
        self.clock.reset()
        self.file_name = file_name
    
    def clear(self):
        with open(self.file_name,'w+') as _:
            pass

    def push(self, data):
        with open(self.file_name,'a+') as file:
            file.write(','.join(['{}'.format(value) for value in [self.clock.time()/1000.0]+data])+'\n')
